package com.example.administrator.accountsystem_demo;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.internal.widget.ContentFrameLayout;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class Register extends ActionBarActivity {

    private EditText reg_username;
    private EditText reg_userpws;
    private EditText confirm_userpws;
    private EditText reg_usermail;
    private Button btnRegSubmit;
    private Button btnRegBack;
    private Button btnRegReset;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        reg_username = (EditText) findViewById(R.id.reg_username);
        reg_userpws = (EditText) findViewById(R.id.reg_userpws);
        confirm_userpws = (EditText) findViewById(R.id.confirm_userpws);
        reg_usermail = (EditText) findViewById(R.id.reg_usermail);

        btnRegSubmit = (Button) findViewById(R.id.btnRegSubmit);
        btnRegBack = (Button) findViewById(R.id.btnRegBack);
        btnRegReset = (Button) findViewById(R.id.btnRegReset);

        btnRegSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TryRegister();
            }
        });
        btnRegBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        btnRegReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Reset();
            }
        });

    }

    public void Reset(){
        reg_usermail.setText("");
        reg_username.setText("");
        reg_userpws.setText("");
        confirm_userpws.setText("");
    }


    public void TryRegister(){
//        String newName = reg_username.getText().toString();
//        String newPws = reg_userpws.getText().toString();
//        String cfPws = confirm_userpws.getText().toString();
//        String newMail = reg_usermail.getText().toString();
       int isValidname = checkname();
       if (isValidname == -1) {
           Toast.makeText(this,"Username can't be empty!",Toast.LENGTH_LONG).show();
           return;
       }
       int  isValidPws = checkpws();
        if (isValidPws == -1) {
            Toast.makeText(this,"Password can't be empty!",Toast.LENGTH_LONG).show();
            return;
        }
        if (isValidPws == -2){
            Toast.makeText(this,"Two passwords are not identical!",Toast.LENGTH_LONG).show();
            return ;
        }
        int isValidMail = checkmail();
        if(isValidMail == -1){
            Toast.makeText(this,"Email address can't be empty!", Toast.LENGTH_LONG).show();
            return;
        }

        Register();

    }

    private int checkname(){
        if (TextUtils.isEmpty(reg_username.getText().toString()))
            return -1;
        else return 1;
    }

    private int checkpws(){
        String p1 = reg_userpws.getText().toString();
        String p2 = confirm_userpws.getText().toString();
        if (TextUtils.isEmpty(p1) || TextUtils.isEmpty(p2)){
            return -1;
        }
        else if (!p1.equals(p2)){
            return -2;
        }
        else return 1;
    }

    private int checkmail(){
        if (TextUtils.isEmpty(reg_usermail.getText().toString()))
            return -1;
        else return 1;
    }

    private void Register(){
        Toast.makeText(this,"valid to register!", Toast.LENGTH_LONG).show();
    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_register, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
