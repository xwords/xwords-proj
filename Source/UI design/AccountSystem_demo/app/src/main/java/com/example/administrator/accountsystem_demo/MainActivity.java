package com.example.administrator.accountsystem_demo;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.security.PrivateKey;


public class MainActivity extends ActionBarActivity {

    private EditText login_userame;
    private EditText login_password;
    private Button btnLogin;
    private Button btnRegister;
    private Button btnBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        login_userame = (EditText)findViewById(R.id.login_username);
        login_password = (EditText)findViewById(R.id.login_password);
        btnLogin = (Button)findViewById(R.id.btnLogin);
        btnRegister = (Button)findViewById(R.id.btnRegister);
        btnBack = (Button)findViewById(R.id.btnBack);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Trylogin();
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                register();
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }

    private void Trylogin(){
        String name = login_userame.getText().toString();
        String pws = login_password.getText().toString();
        if (TextUtils.isEmpty(name)){
            Toast.makeText(this,"name can't be empty!",Toast.LENGTH_LONG).show();
            return;
        }
        else if (TextUtils.isEmpty(pws)){
            Toast.makeText(this,"password can't be empty!",Toast.LENGTH_LONG).show();
            return;
        }
        login(name,pws);
    }

    private void login(String username, String password) {
        System.out.println(username);
        System.out.println(password);
        Toast.makeText(this,username + " seems to successfully login.",Toast.LENGTH_LONG).show();
        //Toast.makeText(this,password,Toast.LENGTH_LONG).show();
    }
    private void register(){
        Toast.makeText(this,"will go to register page!",Toast.LENGTH_LONG).show();
        Intent i = new Intent(MainActivity.this,Register.class);
        startActivity(i);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
